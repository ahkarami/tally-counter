package sbu.cs;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {

        long fact = 1;
        for (int i = 1; i <= n; i++)
        {
            fact = fact * i;
        }

        return fact;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n) {
        long fib;
        if (n == 1 || n == 2)
        {
            fib = 1;
            return fib;
        }
        else
        {
            fib = fibonacci(n-1) + fibonacci(n-2);
            return fib;
        }
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word) {

        StringBuilder rev = new StringBuilder(word);

        return rev.reverse().toString();
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {

        boolean is = false;
        String newLine = line;

        newLine = newLine.replaceAll("[^a-z|A-Z]", "");
        newLine = newLine.toLowerCase();

        StringBuilder str = new StringBuilder(newLine);
        String reversedLine = str.reverse().toString();

        if (newLine.compareTo(reversedLine) == 0)
        {
            is = true;
        }

        return is;
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2) {

        int row = str1.length();
        int col = str2.length();

        char[][] arr = new char[row][col];
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                if (str1.charAt(i) == str2.charAt(j))
                {
                    arr[i][j] = '*';
                }
                else
                {
                    arr[i][j] = ' ';
                }
            }
        }
        return arr;
    }
}
