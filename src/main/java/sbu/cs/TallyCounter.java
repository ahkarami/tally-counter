package sbu.cs;

public class TallyCounter implements TallyCounterInterface {

    private int counter;

    public TallyCounter() {
        this.counter = 0;
    }

    @Override
    public void count() {

        if (this.counter < 9999) {
            counter++;
        }

    }

    @Override
    public int getValue() {

        return this.counter;
    }

    @Override
    public void setValue(int value) throws IllegalValueException {

        if (value < 0 || value > 9999)
        {
            throw new IllegalValueException();
        }

        this.counter = value;

    }
}
