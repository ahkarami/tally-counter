package sbu.cs;

import java.util.Random;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {

        StringBuilder password = new StringBuilder("");
        String alphabets = "abcdefghijklmnopqrstuvwxyz";
        int pos;// random position in alphabets string

        Random rand = new Random();

        for (int i = 0; i < length; i++)
        {
            pos = rand.nextInt(alphabets.length());
            password.append(alphabets.charAt(pos));
        }

        return password.toString();
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {

        if (length < 3)
        {
            throw new IllegalValueException();
        }

        StringBuilder password = new StringBuilder("");
        String alphabets = "abcdefghijklmnopqrstuvwxyz";
        int pos;// random position in alphabets string

        Random rand = new Random();


        for (int i = 0; i < length; i++) {
            pos = rand.nextInt(alphabets.length());
            password.append(alphabets.charAt(pos));
        }

        String digits = "0123456789";
        String specials = "@*#!$%";


        int posDigit = rand.nextInt(digits.length());// random position in digits string
        // random position in password string builder to set a digit in that
        int posInPassForDigit = rand.nextInt(length);

        password.setCharAt(posInPassForDigit, digits.charAt(posDigit));

        int posSpec = rand.nextInt(specials.length());// random position in specials string

        // random position in password string builder to set a special char in that
        int posInPassForSpec;
        do {
            posInPassForSpec = rand.nextInt(length);
        }
        while (posInPassForSpec == posInPassForDigit);
        password.setCharAt(posInPassForSpec, specials.charAt(posSpec));



        return password.toString();

    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {

        boolean flag = false;

        for (int i = 1; i <= n; i++)
        {
            if (n == fib(i) + bin(fib(i)))
            {
                flag = true;
                break;
            }
        }


        return flag;
    }

    public int fib (int n){

        int fibo;

        if (n == 1 || n == 2)
        {
            fibo = 1;
            return fibo;
        }
        else
        {
            fibo = fib (n-1) + fib (n-2);
        }
        return fibo;
    }

    public int bin (int n){

        int count = 0;
        String binary = Integer.toBinaryString(n);

        for (int i = 0; i < binary.length(); i++)
        {
            if (binary.charAt(i) == '1')
            {
                count++;
            }
        }

        return count;
    }
}
