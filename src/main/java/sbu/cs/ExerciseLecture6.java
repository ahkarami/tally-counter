package sbu.cs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {

        long sum = 0;

        for (int i = 0; i < arr.length; i += 2)
        {
            sum += arr[i];
        }

        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {

        int temp = 0;
        int i = 0;
        int j = arr.length - 1;

        while (i < j)
        {
            temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;

            j--;
            i++;
        }

        return arr;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {

        if (m1[0].length != m2.length)
        {
            throw new RuntimeException();
        }

        double [][] product = new double[m1.length][m2[0].length];

//        int m1_i = 0,m1_j = 0;
//        int m2_i = 0, m2_j = 0;

        for (int i1 = 0; i1 < m1.length; i1++)
        {
            for (int j2 = 0; j2 < m2[0].length; j2++)
            {
                for (int j2i1 = 0; j2i1 < m2.length; j2i1++)
                {
                    product[i1][j2] += m1[i1][j2i1] * m2[j2i1][j2];
                }
            }
        }


        return product;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {

        List<List<String>> list = new ArrayList<>();
        List<String> tmpList = new ArrayList<>();



        for (int i = 0; i < names.length; i++)
        {
            for (int j = 0; j < names[i].length; j++)
            {

                tmpList.add(names [i][j]);

            }


            List<String> clonedTmp = new ArrayList<>(tmpList);
            list.add(clonedTmp);
            tmpList.clear();

        }


        return list;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {

        List<Integer> list = new ArrayList<>();


        if (n == 1)
        {
            return list;
        }

        for (int number = 2; number <= n; number++)
        {
            int tavan = 0;
            while (n % number == 0)
            {
                tavan ++;
                n /= number;
            }
            if (tavan != 0)
            {
                list.add(number);
            }
        }

        return list;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {

        List<String> list = new ArrayList<>();
        String[] splitedLine = line.split(" ");

        for (int i = 0; i < splitedLine.length; i++)
        {
            splitedLine[i] = splitedLine[i].replaceAll("[^a-z|A-Z]", "");
            list.add(splitedLine[i]);
        }


        return list;
    }
}
